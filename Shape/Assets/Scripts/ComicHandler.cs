﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ComicHandler : MonoBehaviour
{

	public int winNumberOfComics;
	//
	public int numberOfComicsFound;
	//
	public bool comicWindowShowing;
	//
	public int currentComicSelected;
	//
	public GameObject comicWindow;
	//
	public Image[] comicPages;


	// Use this for initialization
	void Start ()
	{

		winNumberOfComics = comicPages.Length;
	


	}
	
	// Update is called once per frame
	void Update ()
	{
	
		if (numberOfComicsFound >= winNumberOfComics) {
			print ("Win!");
		}

		if (comicWindowShowing) {

			Time.timeScale = 0;
		

			if (Input.GetKeyDown (KeyCode.Escape)) {
				comicWindow.SetActive (false);
				comicWindowShowing = false;
			}

			for (int i = 0; i < comicPages.Length; i++) {

				if(i == currentComicSelected){
					comicPages[i].enabled = true;
				}else{
					comicPages[i].enabled = false;
				}

			}

		} else {

			Time.timeScale = 1;

			if (Input.GetKeyDown (KeyCode.Escape)) {
				comicWindow.SetActive (true);
				comicWindowShowing = true;
			}
		}
		


	}

	void AddComic ()
	{
		if (numberOfComicsFound != winNumberOfComics) {
			numberOfComicsFound++;
		}
	}

	public	void MoveThroughComics (string direction)
	{

		//cycles up through the array
		if (direction == "Up") {
			if (currentComicSelected == comicPages.Length - 1) {
				currentComicSelected = 0;
			} else {
				currentComicSelected++;
			}
		}

		//cycles down through the array
		if (direction == "Down") {
			if (currentComicSelected == 0) {
				currentComicSelected = comicPages.Length - 1;
			} else {
				currentComicSelected--;
			}

		}

	}

	void OnTriggerEnter(Collider coll){

		if(coll.gameObject.tag == "Comic Page"){
			numberOfComicsFound++;
			Destroy(coll.gameObject);
		}

	}
}
