﻿using UnityEngine;
using System.Collections;

namespace Durian
{
	public class CameraMovement : MonoBehaviour
	{

		public Transform objectToOrbit;
		public Transform camera;
		public float turningSpeed;
		//
		public float cameraDepth, cameraHeight;
		//
		public bool followTransform;


		public Vector3 offset;

		void Start ()
		{


			//sets the offset of the camera
			offset = new Vector3 (objectToOrbit.position.x, objectToOrbit.position.y + cameraHeight, objectToOrbit.position.z - cameraDepth);

		}

		void LateUpdate ()
		{
				//turns mouse according to the movement of the thing
				offset = Quaternion.AngleAxis (Input.GetAxis ("Mouse X") * turningSpeed, Vector3.up) * offset;


			if(followTransform){
				//moves camera to position of player
				transform.position = objectToOrbit.position + offset;

				//faces camera towards the player object
				transform.LookAt (objectToOrbit.position);
			}
		}
	}
}