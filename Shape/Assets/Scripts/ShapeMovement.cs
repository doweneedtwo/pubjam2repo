﻿using UnityEngine;
using System.Collections;

namespace Durian
{
	[ExecuteInEditMode]
	public class ShapeMovement : MonoBehaviour
	{
		[Header ("Basic Movement Properties")]
		public float vertical;
		public float horizontal;
		public float bounceDampeningLevel;
		public float rollMovementSpeed;
		//
		[Header ("Masses")]
		public float groundedMass;
		public float glidingMass;
		public float undergroundMass;
		public float digDownTime;
		//

		//
		[Header ("State Booleans")]
		public bool isGrounded;
		public bool isUnderground;
		public bool isDigging;
		public bool isGliding;
		public bool isBouncing;
		//
		private bool diggingDown;
		private bool diggingUp;
		//
		[Header ("Layer Masks")]
		public LayerMask groundLayer;
		public LayerMask diggingLayer;
		//
		private GameObject terrainToStore;
		//
		[Header ("References")]
		public CameraMovement cameraScript;
		public Rigidbody rigid;

		// Use this for initialization
		void Start ()
		{
			rigid = GetComponent<Rigidbody> ();
		}

		void FixedUpdate ()
		{
			MovePlayer ();

			StateChecker ();

			if (diggingUp && !isUnderground) {
				rigid.velocity = new Vector3 (0, 3, 0);
				diggingUp = false;
				terrainToStore.layer = LayerMask.NameToLayer ("Ground");
				cameraScript.followTransform = true;
			}
		}


		//PLAYER MOVEMENT

		#region

		void MovePlayer ()
		{
			//sets values to the axises
			vertical = Input.GetAxisRaw ("Vertical");
			horizontal = Input.GetAxisRaw ("Horizontal");

			//movementforce determines the speed the ball moves at
			Vector3 movementForce = new Vector3 (horizontal, 0, vertical);
			movementForce = Camera.main.transform.TransformDirection (movementForce.normalized);

			movementForce.y = 0;

			//moves the ball object around
			rigid.AddForce (movementForce.normalized * rollMovementSpeed, ForceMode.Force);




			if (Input.GetKey (KeyCode.Space)) {

				if (horizontal == 0 && vertical == 0) {
					isDigging = true;
					isBouncing = false;
				} else {
					isDigging = false;
					isBouncing = true;
				}

			} else {
				isBouncing = false;
				isDigging = false;
			}

		
			if (Input.GetKey (KeyCode.E)) {

				if (gameObject.transform.localScale.x <= 10f) {
					gameObject.transform.localScale += new Vector3 (0.05f, 0.05f, 0.05f);
				}

			}

			if (Input.GetKey (KeyCode.Q)) {
				if (gameObject.transform.localScale.x >= 0.25f) {
					gameObject.transform.localScale -= new Vector3 (0.05f, 0.05f, 0.05f);
				}
			}

			if (diggingUp) {
				rigid.AddForce (Vector3.up * 2, ForceMode.VelocityChange);
			}

		}

		#endregion

		//GROUNDED CHECKING

		#region


		/// <summary>
		/// Checks for the current state of the player
		/// </summary>
		void StateChecker ()
		{

			RaycastHit hit;
			Debug.DrawRay (transform.position, Vector3.down, Color.green);
			Debug.DrawRay (transform.position, Vector3.up * 10, Color.green);

			//casts ray below player
			if (Physics.Raycast (transform.position, Vector3.down, out hit, 3, groundLayer)) {

				isGrounded = true;
				isGliding = false;
				rigid.mass = groundedMass;

			} else {

				isGrounded = false;
				isGliding = true;

				rigid.mass = glidingMass;

			}

			//casts ray below player
			if (Physics.Raycast (transform.position, Vector3.up * 10, out hit, 100f)) {

				isUnderground = true;
				isGrounded = false;
				rigid.mass = undergroundMass;

			} else {
				isUnderground = false;

			}



		}

		#endregion

		//COLLISION METHODS

		#region

		void OnCollisionEnter (Collision coll)
		{

			if (isBouncing) {

				Vector3 bounceForce = new Vector3 (0, rigid.velocity.y - bounceDampeningLevel, 0);
				rigid.AddForce (bounceForce * 250, ForceMode.Impulse);

			}

		}

		void OnCollisionStay (Collision coll)
		{

			if (isDigging) {
				if (coll.gameObject.tag == "Diggable") {
					cameraScript.followTransform = false;
					Dig (coll.gameObject);
				}
			}

		}

		#endregion


		void Dig (GameObject diggingTerrain)
		{
			if (diggingTerrain.tag == "Diggable") {
				terrainToStore = diggingTerrain;
				terrainToStore.layer = LayerMask.NameToLayer ("Dig");
				diggingDown = true;
				StartCoroutine ("DigDown");
			}

		}

		IEnumerator DigDown ()
		{
			yield return new WaitForSeconds (digDownTime);

			diggingDown = false;
			diggingUp = true;

		}
	}

}