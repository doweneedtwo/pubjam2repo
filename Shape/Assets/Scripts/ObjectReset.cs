﻿using UnityEngine;
using System.Collections;

public class ObjectReset : MonoBehaviour
{

	public Color defaultMaterialColour;
	//
	public Vector3 defaultPosition;
	public Vector3 defaultScale;
	//
	public Quaternion defaultRotation;

	// Use this for initialization
	void Start ()
	{
		defaultRotation = transform.rotation;
		defaultPosition = gameObject.transform.position;
		defaultScale = gameObject.transform.localScale;
        
        if(GetComponent<MeshRenderer>())
		defaultMaterialColour = GetComponent<MeshRenderer>().material.color;

	}
	
	// Update is called once per frame
	void Update ()
	{

		if (Input.GetKeyDown (KeyCode.R)) {

			StopAllCoroutines();

            if (GetComponent<MeshRenderer>())
                GetComponent<MeshRenderer>().material.color = defaultMaterialColour;

			gameObject.transform.position = defaultPosition;
			gameObject.transform.rotation = defaultRotation;
			gameObject.transform.localScale = defaultScale;

			if(GetComponent<Rigidbody>()){

				GetComponent<Rigidbody>().velocity = Vector3.zero;

			}


			if (gameObject.GetComponent<CanBurn> () == true) {

				gameObject.GetComponent<CanBurn> ().isBurning = gameObject.GetComponent<CanBurn> ().isBurningDefault;
				gameObject.GetComponent<CanBurn> ().cantBurn = false;
				gameObject.GetComponent<CanBurn>().StopAllCoroutines();
			}


			if (gameObject.GetComponent<CanZap> () == true) {

				gameObject.GetComponent<CanZap> ().isZapping = gameObject.GetComponent<CanZap> ().isZappingDefault;
				gameObject.GetComponent<CanZap> ().cantZap = false;
				gameObject.GetComponent<CanZap>().StopAllCoroutines();

			}

			if (gameObject.GetComponent<CanStick> () == true) {

				gameObject.GetComponent<CanStick> ().isSticking = gameObject.GetComponent<CanStick> ().isStickingDefault;
				gameObject.GetComponent<CanStick> ().cantStick = false;
				gameObject.GetComponent<CanStick>().StopAllCoroutines();

			}


			if (gameObject.GetComponent<CanSlip> () == true) {

				gameObject.GetComponent<CanSlip> ().isSlipping = gameObject.GetComponent<CanStick> ().isStickingDefault;
				gameObject.GetComponent<CanSlip> ().cantSlip = false;
				gameObject.GetComponent<CanSlip>().StopAllCoroutines();

			}


            if (gameObject.GetComponent<Roaming>() == true)
            {
               
            }









            }

        }
}
