﻿using UnityEngine;
using System.Collections;

public class CanBurn : MonoBehaviour
{
	public bool isBurningDefault;

    public bool isBurning;
    public bool cantBurn;
    //
    public Color burnColour1;
    public Color burnColour2;


    void Start()
    {
		
        //boolean to run method only once when fire starts
        cantBurn = false;


    }

    void Update()
    {
		
        //if the object is set on fire and is able to be set on fire for the first time
        if (isBurning && !cantBurn)
        {
            StartCoroutine(BurnObject());
            //enables safety boolean to prevent slowdown and re-running methods
            cantBurn = true;
        }

    }


    //coroutine triggered when object starts its burn
    public IEnumerator BurnObject()
    {
        //marks object as burning
        isBurning = true;

        GetComponent<MeshRenderer>().material.color = burnColour1;

        //randomizes value in order to start next coroutine at a staggered rate
        float randomBurnStagger = Random.Range(3f, 6f);

        //waits before continuing coroutine
        yield return new WaitForSeconds(randomBurnStagger);

        //changes material (VFX changes will take place here)
        GetComponent<MeshRenderer>().material.color = burnColour2;


        //Starts next iteration of the cycle
        //StartCoroutine("BurnObjectDestruction");
    }

    //timer to destroy object after its finished burning
    IEnumerator BurnObjectDestruction()
    {
        float randomBurnStagger = Random.Range(3f, 6f);

        yield return new WaitForSeconds(randomBurnStagger);

        //destroy the burning object
        Destroy(gameObject);
    }


    void OnCollisionEnter(Collision coll)
    {
        if (coll.gameObject.GetComponent<CanBurn>())
        {
            //if the touching game object has this scritp and isn't burning
            if (!coll.gameObject.GetComponent<CanBurn>().isBurning)
            {
                //if this object is burning
				if (isBurning && coll.gameObject.GetComponent<CanBurn>() == enabled)
                {

                    CanBurn objectBurnScript = coll.gameObject.GetComponent<CanBurn>();

                    //alights object touching this one
                    objectBurnScript.isBurning = true;

                    //sets colour variables in object touched to those of this object
                    objectBurnScript.burnColour1 = burnColour1;
                    objectBurnScript.burnColour2 = burnColour2;

                    //burn it to the ground!
                    StartCoroutine(coll.gameObject.GetComponent<CanBurn>().BurnObject());
                }
            }
        }
    }

}
