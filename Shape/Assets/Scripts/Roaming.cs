﻿using UnityEngine;
using System.Collections;

public class Roaming : MonoBehaviour
{

    public NavMeshAgent navAgent;
    //
    public Vector3 currentTarget;
    public float wanderSpread;
    //
    public int wanderTime;

    // Use this for initialization
    void Start()
    {

        navAgent = GetComponent<NavMeshAgent>();
        StartCoroutine("FindNewPoint");

    }

    // Update is called once per frame
    void Update()
    {


        navAgent.destination = currentTarget;


    }


    IEnumerator FindNewPoint()
    {
        float pointZ = UnityEngine.Random.Range(-wanderSpread, wanderSpread);
        float pointX = UnityEngine.Random.Range(-wanderSpread, wanderSpread);

        float wandertime = UnityEngine.Random.Range(-wanderTime, wanderTime);


        currentTarget = new Vector3(pointX + transform.position.x, transform.position.y, pointZ + transform.position.z);

        yield return new WaitForSeconds(wandertime);

        StartCoroutine("FindNewPoint");


    }






    void OnCollisionEnter(Collision coll)
    {


    }


    void OnTriggerEnter(Collider coll)
    {

    }

    void OnTriggerExit(Collider coll)
    {


    }

}
