﻿using UnityEngine;
using System.Collections;

public class CanStick : MonoBehaviour
{
	public bool isStickingDefault;
	public bool kinematicDefault;
	//
	public bool isSticking;
	public bool cantStick;
	//


	void Start ()
	{
		
		//boolean to run method only once when fire starts
		cantStick = false;

		kinematicDefault = GetComponent<Rigidbody>().isKinematic;
	}

	void Update ()
	{
		
		//if the object is set on fire and is able to be set on fire for the first time
		if (isSticking && !cantStick) {
			//enables safety boolean to prevent slowdown and re-running methods
			cantStick = true;
		}

		if(isSticking){

			GetComponent<MeshRenderer>().material.color = Color.black;

			GetComponent<Rigidbody>().isKinematic = true;


		}else{
			GetComponent<Rigidbody>().isKinematic = kinematicDefault;
		}

	}

	void OnCollisionStay (Collision coll)
	{
		if (coll.gameObject.GetComponent<CanStick> ()) {
			//if the touching game object has this scritp and isn't zapped
			if (!coll.gameObject.GetComponent<CanStick> ().isSticking) {
				//if this object is burning
				if (isSticking && coll.gameObject.GetComponent<CanStick> () == enabled) {

					CanStick objectStickScript = coll.gameObject.GetComponent<CanStick> ();

					//alights object touching this one
					objectStickScript.isSticking = true;
				
				}
			}

		}
	}

}
