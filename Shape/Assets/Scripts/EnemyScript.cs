﻿using UnityEngine;
using System.Collections;

public class EnemyScript : MonoBehaviour {

    public NavMeshAgent navAgent;
    //
    public GameObject currentTarget;
    //
    public bool guardPosition;
    //
    public Vector3 startLocation;
    //
    public Transform jailLocation;

	// Use this for initialization
	void Start () {

        navAgent = GetComponent<NavMeshAgent>();

        startLocation = transform.position;

	}

    // Update is called once per frame
    void Update()
    {

        if(currentTarget != null)
        {

            navAgent.destination = currentTarget.transform.position;

        }
        else
        {
            if (guardPosition)
            {
                navAgent.destination = startLocation;
            }

        }


    }

    void OnCollisionEnter(Collision coll)
    {
        if (coll.gameObject == currentTarget && coll.gameObject.tag != "Player")
        {

            coll.transform.position = jailLocation.position;
            currentTarget = null;

        }


    }
    

    void OnTriggerEnter(Collider coll)
    {

        if(coll.gameObject.layer == LayerMask.NameToLayer("Chased") || coll.gameObject.tag == "Player")
        {
            currentTarget = coll.gameObject;
        }

    }

    void OnTriggerExit(Collider coll)
    {

        if (coll.gameObject == currentTarget)
        {

            currentTarget = null;

        }

    }

}
