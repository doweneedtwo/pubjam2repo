﻿using UnityEngine;
using System.Collections;

public class CanSlip : MonoBehaviour
{
	public bool isSlippingDefault;
	public float staticDefault;
	public float dynamicDefault;
	//
	public bool isSlipping;
	public bool cantSlip;
	//


	void Start ()
	{
		
		//boolean to run method only once when fire starts
		cantSlip = false;

		staticDefault = GetComponent<Collider>().material.staticFriction;
		dynamicDefault = GetComponent<Collider>().material.dynamicFriction;
	}

	void Update ()
	{
		
		//if the object is set on fire and is able to be set on fire for the first time
		if (isSlipping && !cantSlip) {
			//enables safety boolean to prevent slowdown and re-running methods
			cantSlip = true;
		}

		if(isSlipping){

			GetComponent<MeshRenderer>().material.color = Color.black;

			GetComponent<Collider>().material.dynamicFriction = 0;
			GetComponent<Collider>().material.staticFriction = 0;


		}else{
			GetComponent<Collider>().material.dynamicFriction = dynamicDefault;
			GetComponent<Collider>().material.staticFriction = staticDefault;
		}

	}

	void OnCollisionStay (Collision coll)
	{
		if (coll.gameObject.GetComponent<CanSlip> ()) {
			//if the touching game object has this scritp and isn't zapped
			if (!coll.gameObject.GetComponent<CanSlip> ().isSlipping) {
				//if this object is burning
				if (isSlipping && coll.gameObject.GetComponent<CanSlip> () == enabled) {

					CanSlip objectSlipScript = coll.gameObject.GetComponent<CanSlip> ();

					//alights object touching this one
					objectSlipScript.isSlipping = true;
				
				}
			}

		}
	}

}
