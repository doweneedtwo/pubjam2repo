﻿using UnityEngine;
using System.Collections;

public class CanZap : MonoBehaviour
{
	public bool isZappingDefault;

	public bool isZapping;
	public bool cantZap;
	//
	public Color zapColour1;


	void Start ()
	{
		
		//boolean to run method only once when fire starts
		cantZap = false;


	}

	void Update ()
	{
		
		//if the object is set on fire and is able to be set on fire for the first time
		if (isZapping && !cantZap) {
			//enables safety boolean to prevent slowdown and re-running methods
			cantZap = true;



			//changes material (VFX changes will take place here)
			GetComponent<MeshRenderer> ().material.color = zapColour1;

		}

	}

	void OnCollisionStay (Collision coll)
	{
		if (coll.gameObject.GetComponent<CanZap> ()) {
			//if the touching game object has this scritp and isn't zapped
			if (!coll.gameObject.GetComponent<CanZap> ().isZapping) {
				//if this object is burning
				if (isZapping && coll.gameObject.GetComponent<CanZap> () == enabled) {

					CanZap objectZapScript = coll.gameObject.GetComponent<CanZap> ();

					//alights object touching this one
					objectZapScript.isZapping = true;


					//changes material (VFX changes will take place here)
					//GetComponent<MeshRenderer> ().material.color = zapColour1;
				}
			}

		}
	}
		

}
